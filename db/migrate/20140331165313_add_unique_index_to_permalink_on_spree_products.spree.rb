# This migration comes from spree (originally 20130628021056)
class AddUniqueIndexToPermalinkOnSpreeProducts < ActiveRecord::Migration
  def change
  	return if index_exists?(:spree_products, :permalink)
    add_index "spree_products", ["permalink"], :name => "permalink_idx_unique", :unique => true
  end
end
