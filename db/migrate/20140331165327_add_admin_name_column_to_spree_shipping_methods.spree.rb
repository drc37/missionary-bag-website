# This migration comes from spree (originally 20130809164245)
class AddAdminNameColumnToSpreeShippingMethods < ActiveRecord::Migration
  def change
  	return if column_exists?(:spree_shipping_methods, :admin_name)
    add_column :spree_shipping_methods, :admin_name, :string
  end
end
