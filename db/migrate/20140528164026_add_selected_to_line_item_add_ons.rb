class AddSelectedToLineItemAddOns < ActiveRecord::Migration
  def change
  	add_column :spree_line_item_add_ons, :selected, :boolean, :default => false
  end
end
