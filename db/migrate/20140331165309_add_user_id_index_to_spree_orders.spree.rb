# This migration comes from spree (originally 20130611185927)
class AddUserIdIndexToSpreeOrders < ActiveRecord::Migration
  def change
  	return if index_exists?(:spree_orders, :user_id)
    add_index :spree_orders, :user_id
  end
end
