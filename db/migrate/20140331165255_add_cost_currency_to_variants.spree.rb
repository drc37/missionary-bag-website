# This migration comes from spree (originally 20121109173623)
class AddCostCurrencyToVariants < ActiveRecord::Migration
  def change
  	return if column_exists?(:spree_variants, :cost_currency)

    add_column :spree_variants, :cost_currency, :string, :after => :cost_price
  end
end
