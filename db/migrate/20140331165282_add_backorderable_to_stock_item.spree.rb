# This migration comes from spree (originally 20130306195650)
class AddBackorderableToStockItem < ActiveRecord::Migration
  def change
  	return if column_exists?(:spree_stock_items, :backorderable)
    add_column :spree_stock_items, :backorderable, :boolean, :default => true
  end
end
