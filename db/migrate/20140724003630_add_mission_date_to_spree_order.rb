class AddMissionDateToSpreeOrder < ActiveRecord::Migration
  def change
  	add_column :spree_orders, :mission_date, :string
  end
end
