# This migration comes from spree (originally 20121126040517)
class AddLastIpToSpreeOrders < ActiveRecord::Migration
  def change
  	return if column_exists?(:spree_orders, :last_ip_address)
    add_column :spree_orders, :last_ip_address, :string
  end
end
