# This migration comes from spree (originally 20130306191917)
class AddActiveFieldToStockLocations < ActiveRecord::Migration
  def change
  	return if column_exists?(:spree_stock_locations, :active)
    add_column :spree_stock_locations, :active, :boolean, :default => true
  end
end
