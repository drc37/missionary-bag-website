# This migration comes from spree (originally 20121107194006)
class AddCurrencyToOrders < ActiveRecord::Migration
  def change
  	return if column_exists?(:spree_orders, :currency)
  	
    add_column :spree_orders, :currency, :string
  end
end
