# This migration comes from spree (originally 20130809164330)
class AddAdminNameColumnToSpreeStockLocations < ActiveRecord::Migration
  def change
  	return if column_exists?(:spree_stock_locations, :admin_name)
    add_column :spree_stock_locations, :admin_name, :string
  end
end
