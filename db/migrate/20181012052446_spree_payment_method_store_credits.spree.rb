# This migration comes from spree (originally 20150714154102)
class SpreePaymentMethodStoreCredits < ActiveRecord::Migration
  def up
    # Reload to pick up new position column for acts_as_list
    Spree::PaymentMethod.reset_column_information
   #  if column_exists?(:spree_payment_methods, :display_on)
	  #   Spree::PaymentMethod::StoreCredit.find_or_create_by(name: 'Store Credit', description: 'Store Credit',
   #                                                      active: true, display_on: 'back_end')
	  # else 
	  # 	Spree::PaymentMethod::StoreCredit.find_or_create_by(name: 'Store Credit', description: 'Store Credit',
   #                                                      active: true)
	  # end
  end

  def down
    Spree::PaymentMethod.find_by(type: 'Spree::PaymentMethod::StoreCredit', name: 'Store Credit').try(&:destroy)
  end
end
