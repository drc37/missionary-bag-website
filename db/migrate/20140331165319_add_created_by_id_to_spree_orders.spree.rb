# This migration comes from spree (originally 20130725031716)
class AddCreatedByIdToSpreeOrders < ActiveRecord::Migration
  def change
  	return if column_exists?(:spree_orders, :created_by_id)
    add_column :spree_orders, :created_by_id, :integer
  end
end
