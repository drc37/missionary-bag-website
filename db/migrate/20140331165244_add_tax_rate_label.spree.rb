# This migration comes from spree (originally 20120905145253)
class AddTaxRateLabel < ActiveRecord::Migration
  def change
  	return if column_exists?(:spree_tax_rates, :name)

    add_column :spree_tax_rates, :name, :string
  end
end
