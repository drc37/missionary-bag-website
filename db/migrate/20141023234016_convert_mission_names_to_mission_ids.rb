class ConvertMissionNamesToMissionIds < ActiveRecord::Migration
  def up
    Spree::Order.all.each do |order|
      if order.mission.present?
        mis = Mission.find_by(:mission_name => order.mission)
        if mis.blank?
          order.update_attribute(:other, order.mission)
        else
          order.update_attribute(:mission, mis.id)
        end
      end
    end
  end

  def down
    Spree::Order.all.each do |order|
      if order.mission.present?
        mis = Mission.find(order.mission)
        order.update_attribute(:mission, mis.mission_name)
      end
    end
  end
end
