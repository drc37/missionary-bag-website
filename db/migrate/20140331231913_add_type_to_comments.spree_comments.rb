# This migration comes from spree_comments (originally 20100406100728)
class AddTypeToComments < ActiveRecord::Migration
  def self.up
  	return if column_exists?(:spree_comments, :comment_type_id)
    add_column :spree_comments, :comment_type_id, :integer
  end

  def self.down
    remove_column :spree_comments, :comment_type_id
  end
end