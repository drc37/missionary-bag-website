# This migration comes from spree_comments (originally 20091021182639)
class CreateComments < ActiveRecord::Migration
  def self.up
    return if table_exists?(:spree_comments)

    create_table :spree_comments do |t|
      t.string :title, :limit => 50, :default => "" 
      t.text :comment, :default => "" 
      t.references :commentable, :polymorphic => true
      t.references :user

      t.timestamps
    end

    add_index :spree_comments, :commentable_type
    add_index :spree_comments, :commentable_id
    add_index :spree_comments, :user_id
  end

  def self.down
    drop_table :spree_comments
  end
end
