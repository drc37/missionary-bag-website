class ConvertMissionToInteger < ActiveRecord::Migration
  def up
    rename_column :spree_orders, :mission, :mission_id
  end

  def down
    rename_column :spree_orders, :mission_id, :mission
  end
end
