class AnotherConversionTool < ActiveRecord::Migration
  def up
    Spree::Order.where("NOT mission_id is NULL").each do |order|
      ap order.mission_id
      if order.mission_id.present? && !(order.mission_id.is_a? Numeric) && order.mission_id.length > 3
        order.update_attribute(:other, order.mission_id)
        order.update_attribute(:mission_id, nil)
      end
    end
    
    # change_column :spree_orders, :mission_id, :integer
    change_column :spree_orders, :mission_id, 'integer USING CAST(mission_id AS integer)'
  end

  def down
    change_column :spree_orders, :mission_id, :string
  end
end
