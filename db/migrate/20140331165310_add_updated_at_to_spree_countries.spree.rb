# This migration comes from spree (originally 20130618041418)
class AddUpdatedAtToSpreeCountries < ActiveRecord::Migration
  def up
  	return if column_exists?(:spree_countries, :updated_at)
    add_column :spree_countries, :updated_at, :datetime
  end

  def down
    remove_column :spree_countries, :updated_at
  end
end
