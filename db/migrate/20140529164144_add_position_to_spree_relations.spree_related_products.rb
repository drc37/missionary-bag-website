# This migration comes from spree_related_products (originally 20130727004612)
class AddPositionToSpreeRelations < ActiveRecord::Migration
	def change
		return if column_exists?(:spree_relations, :position)
  	add_column :spree_relations, :position, :integer
	end
end
