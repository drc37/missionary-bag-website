# This migration comes from spree (originally 20121124203911)
class AddPositionToTaxonomies < ActiveRecord::Migration
  def change
  	return if column_exists?(:spree_taxonomies, :position)
  	
  	add_column :spree_taxonomies, :position, :integer, :default => 0
  end
end
