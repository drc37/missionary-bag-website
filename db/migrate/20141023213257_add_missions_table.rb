class AddMissionsTable < ActiveRecord::Migration
  def change
    create_table :missions do |t|
      t.string :mission_name
      t.string :code
      t.string :country
      t.string :province
      
      t.timestamps
    end
  end
end
