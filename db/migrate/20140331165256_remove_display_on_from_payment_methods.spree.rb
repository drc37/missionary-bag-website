# This migration comes from spree (originally 20121111231553)
class RemoveDisplayOnFromPaymentMethods < ActiveRecord::Migration
  def up
  	return unless column_exists?(:spree_payment_methods, :display_on)

    remove_column :spree_payment_methods, :display_on
  end
end
