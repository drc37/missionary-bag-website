# This migration comes from spree (originally 20130227143905)
class AddPendingToInventoryUnit < ActiveRecord::Migration
  def change
  	return if column_exists?(:spree_inventory_units, :pending)

    add_column :spree_inventory_units, :pending, :boolean, :default => true
    Spree::InventoryUnit.update_all(:pending => false)
  end
end
