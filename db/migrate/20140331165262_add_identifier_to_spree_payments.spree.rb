# This migration comes from spree (originally 20130203232234)
class AddIdentifierToSpreePayments < ActiveRecord::Migration
  def change
  	return if column_exists?(:spree_payments, :identifier)
    add_column :spree_payments, :identifier, :string
  end
end
