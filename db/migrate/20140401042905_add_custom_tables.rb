class AddCustomTables < ActiveRecord::Migration
  def change
  	return if table_exists?(:quickbooks_connections)

  	create_table :quickbooks_connections do |t|
  		t.string :token
			t.string :secret
			t.string :realm_id
			t.string :base_uri
			
			t.timestamps
  	end

  	add_column :spree_orders, :mission, :string
  	add_column :spree_orders, :other, :string

  	add_column :spree_orders, :exported_to_quickbooks, :boolean, :default => false
  	add_column :spree_orders, :quickbooks_customer_id, :integer
  	add_column :spree_orders, :quickbooks_invoice_id, :integer
  	add_column :spree_users, :quickbooks_id, :integer
  	add_column :spree_payments, :quickbooks_id, :integer

  	add_column :spree_orders, :postage_date, :datetime
  	add_column :spree_orders, :postage_id, :string
  	add_column :spree_orders, :postage_type, :string
  	add_column :spree_orders, :postage_file_type, :string
  	add_column :spree_orders, :postage_url, :string
  	add_column :spree_orders, :postage_pdf_url, :string
  	add_column :spree_orders, :postage_epl2_url, :string
  	add_column :spree_orders, :postage_zpl_url, :string
  end
end
