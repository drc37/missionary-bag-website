# This migration comes from spree (originally 20130413230529)
class AddNameToSpreeCreditCards < ActiveRecord::Migration
  def change
  	return if column_exists?(:spree_credit_cards, :name)
    add_column :spree_credit_cards, :name, :string
  end
end
