class AddForceShowToHoggan < ActiveRecord::Migration
  def change
    add_column :spree_orders, :force_to_hoggan, :boolean, :default => false
  end
end
