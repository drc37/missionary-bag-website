# This migration comes from spree (originally 20130514151929)
class AddSkuIndexToSpreeVariants < ActiveRecord::Migration
  def change
  	return if index_exists?(:spree_variants, :sku)
    add_index :spree_variants, :sku
  end
end
