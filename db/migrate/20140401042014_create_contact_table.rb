class CreateContactTable < ActiveRecord::Migration
  def change
  	return if table_exists?(:contacts)

    create_table :contacts do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.text :message
      t.string :marketing_message
			t.boolean :requested_contact

      t.timestamps
    
    end
  end
end
