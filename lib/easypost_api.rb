class EasypostApi
	require 'easypost'

	def self.get_key
	  EasyPost.api_key = ENV['MISSIONARY_BAGS_EASYPOST_KEY']
	end

	def self.get_shipment
		self.get_key

		to_address = EasyPost::Address.create(:name => 'David Cook',:street1 => '140 E 100 S',:street2 => '',:city => 'Burley',:state => 'ID',:zip => '83318',:country => 'USA',:phone => '2693504760')
		from_address = EasyPost::Address.create(:company => 'EasyPost',:street1 => '5128 Geary Blvd',:street2 => '',:city => 'San Francisco',:state => 'CA',:zip => '94118',:phone => '415-379-7678',:email => 'easypostshirts@easypost.com',:size => "XL")

		parcel = EasyPost::Parcel.create(:width => 7,:length => 6, :height => 4,:weight => 13)

		customs_info = EasyPost::CustomsItem.create(
		  :description => 'Nylon Bag',
		  :quantity => 1,
		  :value => 40,
		  :weight => 37,
		  :hs_tariff_number => 402222,
		  :origin_country => 'us',
		)
		shipment = EasyPost::Shipment.create(
		  :to_address => to_address,
		  :from_address => from_address,
		  :parcel => parcel,
		  # :customs_info => customs_info,
		)

		shipment
	end

	def self.get_rates(order)

		quantity = EasypostApi.bag_quantity(order)
		belt_quantity = EasypostApi.belt_quantity(order)


		height = EasypostApi.box_height(order)
		weight = EasypostApi.order_weight(order)

		EasypostApi.get_custom_rates(order, weight, EasypostApi.box_width(order), EasypostApi.box_length(order), height, true)

	end

	def self.get_custom_rates(order, weight, width, length, height, is_residential)
		self.get_key

		my_location = Spree::StockLocation.first
		from_address = EasyPost::Address.create(
			:company => 'Missionary Bags',
			:street1 => my_location.address1,
			:street2 => my_location.address2,
			:city => my_location.city,
			:state => my_location.state.abbr,
			:zip => my_location.zipcode,
			:phone => my_location.phone,
			:email => 'support@missionarybag.com'
		)

		addr = order.shipping_address
		to_address = EasyPost::Address.create(
			:name => "#{addr.firstname} #{addr.lastname}",
			:street1 => addr.address1,
			:street2 => addr.address2,
			:city => addr.city,
			:state => addr.state.abbr,
			:zip => addr.zipcode,
			:country => addr.country.iso3,
			:phone => addr.phone
		)

		parcel = EasyPost::Parcel.create(
			:width => width,
			:length => length,
			:height => height,
			:weight => weight
		)

		# customs_info = EasyPost::CustomsItem.create(
		#   :description => 'Nylon Bag',
		#   :quantity => quantity,
		#   :value => 40,
		#   :weight => 1.6 * quantity + 0.3 * belt_quantity,
		#   :hs_tariff_number => 402222,
		#   :origin_country => 'us',
		# )

		h1 = {
			:to_address => to_address,
		  :from_address => from_address,
		  :parcel => parcel
		}



		if is_residential
			h1 = h1.merge({ :options => { :residential_to_address => 1 }})
		end

		ap h1

		shipment = EasyPost::Shipment.create( h1 )

		shipment.rates
	end

	def self.purchase_postage(order, shipment_id, rate_id)
		self.get_key

		shipment = EasyPost::Shipment.retrieve(shipment_id)
		rate = shipment.rates.select {|a| a.id == rate_id}

		ap shipment
		ap rate
		purchase = shipment.buy(
			:rate => rate.first
		)

		ap purchase
		postage = purchase.postage_label
		tracking_code = purchase.tracking_code
		ap shipment_id
		ap postage
		# Save the purchase info
		order.update_attributes(
			:postage_date => postage.label_date,
			:postage_id => shipment_id,
			:postage_type => postage.label_type,
			:postage_file_type => postage.label_file_type,
			:postage_url => postage.label_url,
			:postage_pdf_url => postage.label_pdf_url,
			:postage_epl2_url => postage.label_epl2_url,
			:postage_zpl_url => postage.label_zpl_url
		)

		ap order.shipments.where("state = 'pending' OR state = 'ready'")
		ap "tracking_code: #{tracking_code}"
		order.shipments.where("state = 'pending' OR state = 'ready'").each do |s|
			s.update_attributes(:tracking => tracking_code)
			s.ship!
		end
	end

	def self.update_postage(order)
		self.get_key

		shipment = EasyPost::Shipment.retrieve(order.postage_id)
		ap shipment
		postage = shipment.postage_label
		# tracking_code = shipment
		order.update_attributes(
			:postage_date => postage.label_date,
			:postage_type => postage.label_type,
			:postage_file_type => postage.label_file_type,
			:postage_url => postage.label_url,
			:postage_pdf_url => postage.label_pdf_url,
			:postage_epl2_url => postage.label_epl2_url,
			:postage_zpl_url => postage.label_zpl_url
		)
		ap order.shipments.where("state = 'pending' OR state = 'ready'")
		tracking_code = shipment.tracking_code
		ap "tracking_code: #{tracking_code}"
		order.shipments.where("state = 'pending' OR state = 'ready'").each do |s|
			s.update_attributes(:tracking => tracking_code)
			s.ship!
		end
	end

	def self.box_height(order)
		quantity = EasypostApi.bag_quantity(order)
		height = 4
		if quantity > 2
			height = 8
		elsif quantity > 6
			height = 12
		end
		height
	end

	def self.box_width(order)
		16
	end

	def self.box_length(order)
		12
	end

	def self.bag_quantity(order)
		quantity = 0
		order.line_items.each do |item|
			if item.price > 30
				quantity += 1
			end
		end
		quantity
	end

	def self.belt_quantity(order)
		belt_quantity = 0
		order.line_items.each do |item|
			if item.price < 30
				belt_quantity += 1
			end
		end

		# Assume item totals of $100 are a belt included
		if order.item_total == 100
			belt_quantity += 1
		end

		belt_quantity
	end

	def self.order_weight(order)
		q = EasypostApi.bag_quantity(order)
		bq = EasypostApi.belt_quantity(order)
		weight = 16 * (0.7 * q + q * 1.6 + 0.3 * bq)
	end
end
