class Analytics

	def get_item_count
		Spree::LineItem.where("spree_orders.state = 'complete'").all(:joins => [:order, :variant], :select => "variant_id, count(variant_id) as count", :group => [:variant_id]).each {|v| ap "#{v.variant_id} - #{v.count} - #{Spree::Variant.find(v.variant_id).sku}" }
	end

end