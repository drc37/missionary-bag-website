MissionaryWebsite4::Application.config.mb_s3_bucket = ENV['MISSIONARY_BAGS_BUCKET']
MissionaryWebsite4::Application.config.mb_s3_key    = ENV['MISSIONARY_BAGS_KEY']
MissionaryWebsite4::Application.config.mb_s3_secret = ENV['MISSIONARY_BAGS_SECRET']