# Configure Spree Preferences
#
# Note: Initializing preferences available within the Admin will overwrite any changes that were made through the user interface when you restart.
#       If you would like users to be able to update a setting with the Admin it should NOT be set here.
#
# In order to initialize a setting do:
# config.setting_name = 'new value'
Spree.config do |config|
  # config.site_name = "Missionary Bags"

  # Email Setup
	enable_mail_delivery = true
	# config.override_actionmailer_config = false
	# config.mails_from = "support@missionarybag.com"

	# Always send me a copy
	# config.mail_bcc = "support@missionarybag.com"

	# Shipping
	# config.shipping_instructions = true
end

Spree.user_class = "Spree::LegacyUser"

Spree::PermittedAttributes.checkout_attributes << :mission
Spree::PermittedAttributes.checkout_attributes << :mission_date
Spree::PermittedAttributes.checkout_attributes << :other
