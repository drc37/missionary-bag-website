MissionaryWebsite4::Application.routes.draw do

  # This line mounts Spree's routes at the root of your application.
  # This means, any requests to URLs such as /products, will go to Spree::ProductsController.
  # If you would like to change where this engine is mounted, simply change the :at option to something different.
  #
  # We ask that you don't use the :as option here, as Spree relies on it being the default of "spree"
  mount Spree::Core::Engine, :at => '/store'

  resources :contacts

  match 'aboutus-us' => 'home#aboutus', :via => [:get], :as => 'about_us'
  match 'privacy' => 'home#privacy', :via => [:get], :as => 'privacy'
  match 'warranty' => 'home#warranty', :via => [:get], :as => 'warranty'
  match 'resellers' => 'home#resellers', :via => [:get], :as => 'resellers'
  match 'almost_finished' => 'home#almost_finished', :via => [:get], :as => 'almost_finished'
  match '' => 'home#index', :via => [:get], :as => 'home'
  resources :hoggans_construction, :only => [:index, :update]
  resources :home do
    member do
      get 'price'
    end
  end
  root :to => 'home#index' 
end

Spree::Core::Engine.routes.draw do
  namespace :admin do
    resources :products do
      resources :add_ons
    end
    resources :reports, :only => [] do
      collection do

        get   :idaho_taxes
        post  :idaho_taxes

      end
    end
    resources :workflows, :only => [:index, :update] do 
      collection do 
        put "update_all"
        get "authenticate"
        put "bluedot"
        get "oauth_callback"
        get "export_to_quickbooks"

      end
      member do
        get "packing_slip"
        get "shipping"
        post "rates"
        get "print_postage"
        post "purchase_postage"
        get "update_shipment"
        get "update_order"
        post 'show_to_hoggans'
        patch 'show_to_hoggans'
      end
    end
  end
end
          # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

