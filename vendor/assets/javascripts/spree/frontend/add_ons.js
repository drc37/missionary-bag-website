$(document).ready(function() {

	$(".chosen-select").chosen({disable_search_threshold: 10});

	$('.datepicker').pickadate();

	$('.buy-now').on("change", function(event){
    // event.preventDefault();
    
  });

	$(".add-on-checkbox").on("change", function(e) {
		console.log($(this).find("input:checkbox"));
		var name = $(this).find("input:checkbox").val();
    var check = $(this).find("input:checkbox").attr('checked');
    console.log("Change: " + name + " to " + check);
    if($(this).children("input:checkbox").is(":checked")){
		  $(this).siblings(".embroidery-options").slideDown();
		} else {
		  $(this).siblings(".embroidery-options").slideUp();
		}
    
	});

  var toggleList = $(".arrows, .embroidery-color, .embroidery-font"),
		list = $(".subject ul"),
		item = list.find("li"),
		subject = $(".embroidery-color");

	toggleList.on("click", function(e){
		e.stopPropagation();
		// 
		// $(".arrows").toggleClass("is-active");
		// list.fadeToggle(); // hide show list
		$(this).siblings("ul").fadeToggle(); // hide show list
	});

	$("body").on("click", function(e){
	  $(".subject ul").fadeOut();
	});

	item.on("click", function(){ // click on item
		console.log(this);
		var itemVal = $(this).find(".val").text();
		$(this).parent().siblings("input").val(itemVal);
		list.fadeOut();
		// item.removeClass("is-active");
		// $(this).addClass("is-active");
		// $(".arrows").removeClass("is-active");
	});

	$(".fancybox").fancybox({
    	openEffect	: 'elastic',
    	closeEffect	: 'elastic',

    	helpers : {
    		title : {
    			type : 'inside'
    		}
    	}
    });
});