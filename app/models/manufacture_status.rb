class ManufactureStatus < ActiveRecord::Base
  # attr_accessible :status, :notes, :line_item_id, :line_item

  belongs_to :line_item, :class_name => "Spree::LineItem", :foreign_key => "line_item_id"	
  has_many :notes, :class_name => :Note, :as => :noteable, :dependent => :destroy
  
end
