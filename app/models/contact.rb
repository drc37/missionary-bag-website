class Contact < ActiveRecord::Base
  # attr_accessible :email, :message, :name, :phone, :marketing_message, :requested_contact

  after_create :notify_admins

  def notify_admins
  	Notifier.contact_submitted(self).deliver_now
  end
end
