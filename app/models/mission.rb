class Mission < ActiveRecord::Base
  # attr_accessible :email, :message, :name, :phone, :marketing_message, :requested_contact
  def self.mission_counts
    
    items =  Spree::Order.joins(:mission).group("missions.country, missions.country").select("count(spree_orders.id) as count, missions.country").order("missions.country")
    counts = {}
    items.each do |item|
      counts.merge!({ "#{item.country}" => item.count}).to_json
    end
    counts
  end
  
end