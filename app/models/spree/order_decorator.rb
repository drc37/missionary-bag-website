module Spree
	Order.class_eval do
		# attr_accessible :mission, :other, :exported_to_quickbooks, :postage_date, :postage_id, :postage_type, :postage_file_type, :postage_url, :postage_pdf_url, :postage_epl2_url, :postage_zpl_url

		has_many :notes, :class_name => :Note, :as => :noteable, :dependent => :destroy
			
		after_save :notify_admins
		belongs_to :mission

		def notify_admins
			if state_changed? && state == "complete"
				ap "Notify Admins"
				Notifier.order_created(self).deliver_now
				
				Notifier.hoggans_order_created(self).deliver_now
			end
		end
	end
end