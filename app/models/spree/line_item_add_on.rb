class Spree::LineItemAddOn < ActiveRecord::Base
  # validates :add_on, :line_item, presence: true

  belongs_to :add_on
  belongs_to :line_item

  delegate :expiration_days, to: :add_on

  # after_create :set_price_and_expiration_date
  before_save :update_price

  def self.line_item_for_add_on(add_on_id_param)
    ap "Add on ID PARAM: #{add_on_id_param}"
    Spree::LineItemAddOn.find_by(:add_on_id => add_on_id_param)
  end

  def expired?
    return false unless add_on.expiration_days

    (self.created_at + add_on.expiration_days.days).past?
  end

  def money
    Spree::Money.new( self.price, currency: currency )
  end

  def purchased!
    add_on.purchased! line_item
  end

  private
  def set_price_and_expiration_date
    ap self
    update_price
    # self.save!
  end

  def update_price
    
    if self.add_on && self.selected
      self.expiration_date = DateTime.current + expiration_days.days if expiration_days

      self.price = self.add_on.price_in(currency).amount
    else
      self.expiration_date = nil
      self.price = 0
    end
  end

  def currency
    self.line_item.order.currency
  end
  
end
