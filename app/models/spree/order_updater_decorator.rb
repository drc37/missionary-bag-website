module Spree
  OrderUpdater.class_eval do
    
    def update_item_total
      order.item_total = 0 
      line_items.each do |li|
        order.item_total += li.amount
      end
      update_order_total
    end
  end
end