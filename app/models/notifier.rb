class Notifier < ActionMailer::Base
	default :from => 'support@missionarybag.com'

  # send a signup email to the user, pass in the user object that contains the user's email address
  def contact_submitted(contact)
    m = mail( :to => 'support@missionarybag.com',
    :subject => "MB - Contact Form: #{contact.email}",
    :body => "<b>Name:</b> #{contact.name} <br> <b>Email:</b> #{contact.email} <br> <b>Phone:</b> #{contact.phone} <br> <b>Message:</b> #{contact.message}".html_safe,
    :content_type => 'text/html')
    # m.deliver
  end

  def order_created(order)
	
  	line_items = ""
  	order.line_items.each do |item|
  		if line_items.length > 0
  			line_items += ", "
  		end
  		line_items += "#{item.product.name} #{item.variant.options_text} #{ item.needs_embroidery? ? " - Needs Emboidery" : ""}<br>"
  	end
  	m = mail( :to => 'support@missionarybag.com',
    :subject => "MB - New Order: #{order.email rescue ""}",
    :body => "<b>Status:</b>#{order.state}<br><b>Number:</b> #{order.number} <br> <b>Email:</b> #{order.email rescue ""} <br> <b>Items Ordered:</b><br>#{line_items}<br><br><br>
    <b>Order Total:</b> #{ ActionController::Base.helpers.number_to_currency(order.total) }
    <br><br>
    https://www.missionarybag.com/store/admin/orders/#{ order.number }/edit
    <br><br>
    <b>Shipping Address</b><br><br>
    First Name: #{order.shipping_address.firstname }<br>
    Last Name: #{order.shipping_address.lastname }<br>
    Address 1: #{order.shipping_address.address1 }<br>
    Address 2: #{order.shipping_address.address2 }<br>
    City: #{order.shipping_address.city }<br>
    State: #{order.shipping_address.state.name }<br>
    Zip: #{order.shipping_address.zipcode }<br>
    Country: #{order.shipping_address.country.name }<br>
    ".html_safe,
    :content_type => 'text/html')
    # m.deliver
	  
	rescue => e
	  ap "error: #{e}" 
  end

  def hoggans_order_created(order)
    send_email = false
    order.line_items.each do |line_item|
      if (line_item.variant.stock_items.count > 0 && !line_item.variant.stock_items.first.in_stock?) || (line_item.line_item_add_ons.count > 0 && line_item.line_item_add_ons.first.selected)
        send_email == true
      end
    end
    if (order.force_to_hoggan || send_email)


    	line_items = ""
      order.line_items.each do |item|
    		if line_items.length > 0
    			line_items += ", "
    		end
        item.line_item_add_ons.each do |lion| 
          if lion.selected && lion.embroidery_text.present?
            is_present = true
          end 
        end 
    		line_items += "#{item.quantity > 1 ? "#{item.quantity} x " : "" }#{item.product.name} - #{item.variant.options_text} #{ item.needs_embroidery? ? " - Needs Emboidery" : ""}<br>"
    	end


    	m = mail( :to => 'kevin@hoggans.com',
    		:cc => "support@missionarybag.com",
      :subject => "MB - New Order for Hoggans",
      :body => "<b>Number:</b> #{order.number} <br> <b>Items Ordered:</b><br> #{line_items}<br><br><br>
      <br><br>
      See complete list and acknowledge:<br>
      https://www.missionarybag.com/hoggans_construction
      <br><br>
      ".html_safe,
      :content_type => 'text/html')
      # m.deliver
    else
      ap "Not sending to hoggans"
      return
    end
  
	rescue => e
	  ap "error: #{e}" 
  end
end