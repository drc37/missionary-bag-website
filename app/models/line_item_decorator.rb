Spree::LineItem.class_eval do
	
	has_one :manufacture_status
	accepts_nested_attributes_for :manufacture_status

	has_many :notes
	
end