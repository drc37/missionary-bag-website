Deface::Override.new(:virtual_path => "spree/orders/show", 
                     :name => "add facebook facebook_conversion_tracker",
                     :insert_after => "fieldset", 
                     :partial => "shared/facebook_conversion_tracker") 

Deface::Override.new(:virtual_path => "spree/checkout/_address", 
                     :name => "add_mission_selector",
                     :insert_before => "div[data-hook='billing_fieldset_wrapper']", 
                     :partial => "orders/mission_selector") 

Deface::Override.new(:virtual_path => "spree/checkout/_delivery", 
                     :name => "Add Addon Number to delivery checkout	",
                     :replace => ".stock-item", 
                     :partial => "spree/add_ons/checkout_delivery_with_add_ons") 

Deface::Override.new(:virtual_path => "spree/shared/_order_details", 
                     :name => "Add Addon to confirmation checkout	",
                     :insert_bottom => "[data-hook='order_item_description']", 
                     :partial => "spree/add_ons/checkout_confirmation_with_add_ons") 
