Deface::Override.new(
	:virtual_path => "spree/products/show", 
	:name => "product-header-remove-old title",
	:remove => ".product-title"
) 
Deface::Override.new(
	:virtual_path => "spree/products/show", 
	:name => "product-header12",
	:insert_top => "div[data-hook='product_show']", 
	:partial => "products/show_header"
) 

Deface::Override.new(
	:virtual_path => "spree/products/show", 
	:name => "change up left column width",
	:set_attributes => "div[data-hook='product_left_part']", 
	:attributes => { :class => "columns eight alpha" }
) 

Deface::Override.new(
	:virtual_path => "spree/products/show", 
	:name => "change up right column width",
	:set_attributes => "div[data-hook='product_right_part']", 
	:attributes => { :class => "columns eight alpha" }
) 

Deface::Override.new(
	:virtual_path => "spree/products/_cart_form", 
	:name => "Add shipping availability to product page (_cart_form)",
	:insert_after => ".variant-description", 
	:partial => "products/product_availability"
) 



# Make a Call Me button
# Deface::Override.new(
# 	:virtual_path => "spree/products/_cart_form", 
# 	:name => "insert a call now",
# 	:replace => ".add-to-cart", 
# 	:text => "<div class='call-us-text center'>To order, Call us now: <div class='our-phone bold'>269-350-4760</div></div>"
# ) 
