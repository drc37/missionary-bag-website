Deface::Override.new(:virtual_path => "spree/layouts/spree_application", 
                     :name => "header-main",
                     :insert_before => ".container", 
                     :partial => "shared/header") 

# Deface::Override.new(:virtual_path => "spree/layouts/spree_application", 
#                      :name => "head-css",
#                      :insert_bottom => "head", 
#                      :text => '<link href="/assets/home.css?body=1" media="all" rel="stylesheet" type="text/css"><link href="/assets/layout.css" media="all" rel="stylesheet" type="text/css">') 

Deface::Override.new(:virtual_path => "spree/shared/_header", 
                     :name => "remove-header",
                     :replace_contents => "div#spree-header",
                     :text => ""
                     )
Deface::Override.new(:virtual_path => "spree/shared/_header", 
                     :name => "remove-header-logo",
                     :remove => "header"
                     ) 


