Deface::Override.new(
	:virtual_path => "spree/layouts/spree_application", 
	:name => "google analytics",
	:insert_after => ".footer", 
	:partial => "shared/google_analytics"
) 