# Store Front
Deface::Override.new(
	:virtual_path => "spree/home/index", 
	:name => "mixpanel home page",
	:insert_after => "div[data-hook='homepage_products']", 
	:text => "<script type='text/javascript'>mixpanel.track('Store Front Page');</script>"
) 

# Pproduct Page
Deface::Override.new(
	:virtual_path => "spree/products/show", 
	:name => "mixpanel product  page",
	:insert_after => "div[data-hook='product_show']", 
	:text => "<script type='text/javascript'>mixpanel.track('Product Page - <%= accurate_title %>');</script>"
)

Deface::Override.new(
	:virtual_path => "spree/orders/edit", 
	:name => "mixpanel Cart",
	:insert_before => "h1", 
	:text => "<script type='text/javascript'>mixpanel.track('Cart');</script>"
	)
Deface::Override.new(
	:virtual_path => "spree/checkout/_address", 
	:name => "mixpanel Checkout Address",
	:insert_before => "div[data-hook='billing_fieldset_wrapper']", 
	:text => "<script type='text/javascript'>mixpanel.track('Checkout Address');</script>"
	)
Deface::Override.new(
	:virtual_path => "spree/checkout/_delivery", 
	:name => "mixpanel Checkout Delivery",
	:insert_before => "fieldset", 
	:text => "<script type='text/javascript'>mixpanel.track('Checkout Delivery');</script>"
	)
Deface::Override.new(
	:virtual_path => "spree/home/_payment", 
	:name => "mixpanel Checkout Payment",
	:insert_before => "fieldset#payment", 
	:text => "<script type='text/javascript'>mixpanel.track('Checkout Payment');</script>"
	)
Deface::Override.new(
	:virtual_path => "spree/checkout/_confirm", 
	:name => "mixpanel Checkout Confirm",
	:insert_before => "fieldset", 
	:text => "<script type='text/javascript'>mixpanel.track('Checkout Confirm');</script>"
) 
Deface::Override.new(
	:virtual_path => "spree/checkout/_summary", 
	:name => "mixpanel Checkout Summary",
	:insert_before => "table", 
	:text => "<script type='text/javascript'>mixpanel.track('Checkout Summary');</script>"
) 
Deface::Override.new(
	:virtual_path => "spree/orders/show", 
	:name => "mixpanel Checkout Complete",
	:insert_before => "fieldset", 
	:text => "<script type='text/javascript'>mixpanel.track('Checkout Complete');</script>"
) 