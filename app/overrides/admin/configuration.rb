Deface::Override.new(:virtual_path => "spree/admin/shared/_configuration_menu", 
                     :name => "Add link to start the process for connecting to quickbooks",
                     :insert_bottom => ".sidebar", 
                     :partial => "spree/admin/shared/quickeebook_connector_button")