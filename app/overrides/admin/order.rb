Deface::Override.new(:virtual_path => "spree/admin/orders/_form", 
                     :name => "add_special_instructions",
                     :insert_top => "div[data-hook='admin_order_form_fields']", 
                     :partial => "orders/show") 

Deface::Override.new(:virtual_path => "spree/admin/orders/index", 
                     :name => "Add * if there are special instructions",
                     :insert_top => "#listing_orders tbody tr td:nth-child(2)", 
                     :partial => "orders/index") 
