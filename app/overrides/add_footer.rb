Deface::Override.new(:virtual_path => "spree/layouts/spree_application", 
                     :name => "footer-main",
                     :insert_after => "div.container", 
                     :partial => "shared/footer") 

Deface::Override.new(:virtual_path => "spree/layouts/spree_application", 
                     :name => "Page Wrapper",
                     :surround_contents => "body", 
                     :text => "<div class='page-wrapper'><%= render_original %></div>") 

Deface::Override.new(:virtual_path => "spree/shared/_footer", 
                     :name => "footer-remove",
                     :remove => "footer"
                     ) 