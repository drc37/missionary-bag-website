Deface::Override.new(
	:virtual_path => "spree/home/index", 
	:name => "product-header121",
	:insert_before => "div[data-hook='homepage_products']", 
	:partial => "products/products_header"
) 
Deface::Override.new(
  :virtual_path =>"spree/layouts/spree_application",
  :name => "remove_sidebar2",
  :remove => "erb[loud]:contains('spree/shared/sidebar')")
Deface::Override.new(
  :virtual_path =>"spree/layouts/spree_application",
  :name => "set_products_page_width3",
  :set_attributes => "#content",
  :attributes => {:class => "columns sixteen" })
Deface::Override.new(
  :virtual_path =>"spree/shared/_products",
  :name => "remove_3rd attributes-alpha4",
  :remove_from_attributes => "[data-hook='products_list_item']",
  :attributes => {:class => "alpha"}
  )
Deface::Override.new(
  :virtual_path =>"spree/shared/_products",
  :name => "remove_3rd attributes omega5",
  :remove_from_attributes => "[data-hook='products_list_item']",
  :attributes => {:class => "omega"}
  )
Deface::Override.new(
  :virtual_path =>"spree/shared/_products",
  :name => "remove_3rd attributes secondary6",
  :remove_from_attributes => "[data-hook='products_list_item']",
  :attributes => {:class => "secondary"}
  )
Deface::Override.new(
  :virtual_path => "spree/shared/_products", 
  :name => "make larger image on products page7",
  :replace => "div.product-image", 
  :partial => "products/product_image"
) 

Deface::Override.new(:virtual_path => "spree/products/_cart_form", 
                     :name => "Change name from Variants 8",
                     :replace => "#product-variants .product-section-title", 
                     :text => "<h6 class='product-section-title'>Choose your interior color:</h6>") 
