# Deface::Override.new(:virtual_path => "spree/orders/_form", 
#                      :name => "add_gift_code_field",
#                      :disabled => true
#                      )


Deface::Override.new(:virtual_path => "spree/orders/_form", 
                     :name => "remove-cart-total",
                     :remove => ".cart-total"
                     )

Deface::Override.new(:virtual_path => "spree/orders/_form", 
                     :name => "add related-products to cart",
                     :insert_bottom => "#cart-detail",
                     :partial => "shared/cart_related_products"
                     )

Deface::Override.new(:virtual_path => "spree/orders/_form", 
                     :name => "add_gift_code_field",
                     :partial => "shared/cart_gift_card",
                     :insert_bottom => "#cart-detail"
                     )

Deface::Override.new(:virtual_path => "spree/orders/_form", 
                     :name => "add cart detail",
                     :insert_bottom => "#cart-detail",
                     :partial => "shared/cart_subtotal"
                     )

Deface::Override.new(:virtual_path => "spree/orders/edit", 
                     :name => "remove-old-subtotal",
                     :remove => "#subtotal"
                     )

Deface::Override.new(:virtual_path => "spree/orders/edit", 
                     :name => "remove-old-buttons",
                     :remove => ".sixteen.alpha.omega"
                     )

Deface::Override.new(:virtual_path => "spree/orders/edit", 
                     :name => "remove-old-empty_cart",
                     :remove => "#clear_cart_link"
                     )
Deface::Override.new(:virtual_path => "spree/orders/_line_item", 
                     :name => "remove-smallish-product-image",
                     :remove => ".cart-item-image"
                     )
Deface::Override.new(:virtual_path => "spree/orders/_line_item", 
                     :name => "add-bigger-product-image",
                     :insert_top => "tr",
                     :partial => "shared/medium_product_image"
                     )
Deface::Override.new(:virtual_path => "spree/orders/_line_item", 
                     :name => "add-shipping-time-product-in-cart",
                     :insert_after => ".line-item-description",
                     :partial => "shared/cart_product_availability"
                     )






