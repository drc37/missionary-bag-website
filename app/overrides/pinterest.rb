Deface::Override.new(
	:virtual_path => "spree/products/show", 
	:name => "product-pinterest",
	:insert_top => "#product-images", 
	:partial => "products/pinterest_button"
) 