class HomeController < ApplicationController

	def index
		@contact = Contact.new
		messages = ["Signup now to receive discounts, promotions and exclusive offers from MissionaryBag.com",
							  "Interested in receiving discounts, promotions and exclusive offers from MissionaryBag.com?",
							  "Interested in receiving discounts, promotions and exclusive offers from MissionaryBag.com? We have exciting new products coming down the pipeline, right as we speak."]
		@marketing_message = messages.sample(1).first
	end

	def aboutus
	end

	def privacy
	end

	def warranty
	end

	def resellers
	end

	def almost_finished
	end

	def price
		product = params[:id]
		@variant = Spree::Variant.find_by(:id => params[:id])

		render :partial => "product_price"
	end
end