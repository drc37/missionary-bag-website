Spree::CheckoutController.class_eval do
  helper 'spree/products'

  Spree::PermittedAttributes.line_item_attributes << {add_on_ids: [], :line_item_add_ons_attributes => [:id, :add_on_id, :selected, :embroidery_text, :embroidery_font, :embroidery_color] }

end
