Spree::Admin::ReportsController.class_eval do
  before_filter :advanced_reporting_setup, only: [:index]

  def idaho_taxes
    params[:q] = {} unless params[:q]

    if params[:q][:created_at_gt].blank?
      params[:q][:created_at_gt] = Time.zone.now.beginning_of_month
    else
      params[:q][:created_at_gt] = Time.zone.parse(params[:q][:created_at_gt]).beginning_of_day rescue Time.zone.now.beginning_of_month
    end

    if params[:q] && !params[:q][:created_at_lt].blank?
      params[:q][:created_at_lt] = Time.zone.parse(params[:q][:created_at_lt]).end_of_day rescue Time.zone.now
    else
      params[:q][:created_at_lt] = Date.today + 1.day
    end

    params[:q][:available_on_null] = true

    params[:q][:s] ||= "created_at desc"


    
    @orders = Spree::Order.joins([:ship_address => [:state]]).where("spree_orders.state = 'complete' and spree_orders.created_at > ? and spree_orders.created_at < ? AND spree_states.abbr = 'ID'", params[:q][:created_at_gt], params[:q][:created_at_lt])
    @sales_total = 0;
    @tax_total = 0
    @orders.each do |order| 
      @tax_total += order.additional_tax_total + order.included_tax_total
      @sales_total += order.item_total + order.adjustment_total 
    end


    # @orders = Spree::Order.all
    @search = Spree::Order.ransack(params[:q])
  end

  protected

  def advanced_reporting_setup
    Spree::Admin::ReportsController.add_available_report! :idaho_taxes
  end
end