module Spree
  module Admin
    class WorkflowsController < Spree::Admin::BaseController
			require 'easypost_api'

			skip_before_filter :authenticate_user!, :only => [:index, :update]

    	layout "workflow"

    	before_filter :load_options

			def index
				@orders = Spree::Order.where("state = 'complete' AND NOT shipment_state	= 'shipped'").order("created_at ASC")
				# Create Manufacture Status for each item
				@orders.each do |order|
					order.line_items.each do |item|
						if !item.manufacture_status
							ManufactureStatus.create(:line_item => item)
						end
					end
				end
				@orders.reload

			end

			def update
				line_item_id = params[:id] 
				ap line_item_id

				line_item = LineItem.find(line_item_id)
				ap params
				ap params["manufacture_status"]
				ap params["manufacture_status"]["status"]
				line_item.manufacture_status.status = params["manufacture_status"]["status"]
				
				if line_item.save
					render :json => {:result => "success", :html => ""}
				else
					render :json => {:result => "failed", :html => ""}
				end
			end

			def update_order
				@order = Spree::Order.find_by_number(params[:id])
				@order.update!
				redirect_to edit_admin_order_path(@order.number)
			end

			def packing_slip
				@order = Spree::Order.find_by_number(params[:id])
			end

			def shipping

				@order = Spree::Order.find_by_number(params[:id])

				if @order.postage_id.present?
					redirect_to print_postage_admin_workflow_path
				else
					@weight = EasypostApi.order_weight(@order)
					@width = EasypostApi.box_width(@order)
					@height = EasypostApi.box_height(@order)
					@length = EasypostApi.box_length(@order)
					# @rates = EasypostApi.get_rates(@order)
				end
				
			end

			def rates
				@order = Spree::Order.find_by_number(params[:id])
				@weight = params["weight"]
				@width = params[:width]
				@height = params[:height]
				@length = params[:length]
				residential = params["is_residential"]

				ap residential
				ap residential == "1" ? true : false
				@rates = EasypostApi.get_custom_rates(@order, @weight.to_d * 16, @width.to_d, @length.to_d, @height.to_d, residential == "1" ? true : false)
				render :partial => "rates", :locals => {:rates => @rates, :order => @order} 
			end

			def purchase_postage
				@order = Spree::Order.find_by_number(params[:id])
				
				EasypostApi.purchase_postage(@order, params["shipment_id"], params["rate_id"])

				render "print_postage"
			end

			def print_postage
				@order = Spree::Order.find_by_number(params[:id])
			end

			def update_shipment
				@order = Spree::Order.find_by_number(params[:id])
				EasypostApi.update_postage(@order)				
				render "print_postage"
			end

			def load_options
				@status_options = [
                                  ['From Inventory', 'inventory'],
                                  ['Acknowledged', 'acknowledged'],
                                  ['Begin Manufacture', 'begunManuf'],
                                  ['Completed Manufacture', 'completedManuf'],
                                  ['Delivered', 'delivered']
                                ]
			end

			# Used for QuickeeBooks
			def authenticate
				ap params
		    callback = oauth_callback_admin_workflows_url
		    token = $qb_oauth_consumer.get_request_token(:oauth_callback => callback)
		    session[:qb_request_token] = token
		    redirect_to("https://appcenter.intuit.com/Connect/Begin?oauth_token=#{token.token}") and return
		  end			

		  def bluedot
			  # I am saving access_token and access_secret in company table so I will fetch from there
			  access_token = current_company.access_token
			  access_secret = current_company.access_secret
			  consumer = OAuth::AccessToken.new($qb_oauth_consumer, access_token, access_secret)
			  response = consumer.request(:get, "https://appcenter.intuit.com/api/v1/Account/AppMenu")
			  if response && response.body
			    html = response.body
			    render(:text => html) and return
			  end
			end

			def show_to_hoggans
				@order = Spree::Order.find_by_number(params[:id])
				show_hoggans = params[:order][:force_to_hoggan]
				@order.update_attributes(:force_to_hoggan => show_hoggans)
				if show_hoggans
					Notifier.hoggans_order_created(@order).deliver_now
				end
				redirect_to :back
			end

			def oauth_callback
		    at = session[:qb_request_token].get_access_token(:oauth_verifier => params[:oauth_verifier])
		    token = at.token
		    secret = at.secret
		    realm_id = params['realmId']
		    ap token
		    ap secret
		    ap realm_id
		    qc = QuickbooksConnection.first
		    # store the token, secret & RealmID somewhere for this user, you will need all 3 to work with Quickeebooks
		    if qc 
		    	qc.update_attributes(:token => token, :secret => secret, :realm_id => realm_id)
		    else
		    	qc = QuickbooksConnection.create(:token => token, :secret => secret, :realm_id => realm_id)
		    end
		    
		    # Save the Base URL
		    consumer = OAuth::AccessToken.new($qb_oauth_consumer, token, secret)
			  response = consumer.request(:get, "https://qbo.intuit.com/qbo1/rest/user/v2/" + realm_id)

			  base = (Hash.from_xml response.body)["QboUser"]["CurrentCompany"]["BaseURI"]
			  qc.update_attributes(:base_uri => base)	
			end

			def export_to_quickbooks
				# If order has user, check if the user has been exported to the customer table
				qc = QuickbooksConnection.first
				oauth_client = OAuth::AccessToken.new($qb_oauth_consumer, qc.token, qc.secret)

				# For each non-exported completed Order, export it to quickbooks
				orders = Spree::Order.complete.where("exported_to_quickbooks is false AND shipment_state = 'shipped'")

				
				orders.each do |order|
				# order = orders[0]
					order_email = Quickeebooks::Online::Model::Email.new(order.email)
					
					customer_service = Quickeebooks::Online::Service::Customer.new
					customer_service.access_token = oauth_client
					customer_service.realm_id = qc.realm_id
					customer_service.list

					customer = Quickeebooks::Online::Model::Customer.new
					full_name = "#{order.billing_address.firstname} #{order.billing_address.lastname}"
					customer.name = "#{full_name} - #{order.number}"
					customer.given_name = order.billing_address.firstname
					customer.family_name = order.billing_address.lastname
					customer.email = order_email
					customer.dba_name = full_name
					
					begin
						c = customer_service.create(customer)	
						order.update_attribute(:quickbooks_customer_id, c.id)				
					rescue Exception => error
						ap error
						# User already exists, go find it
						customer_service = Quickeebooks::Online::Service::Customer.new
						customer_service.access_token = oauth_client
						customer_service.realm_id = qc.realm_id
						customer_service.list

						filters = []
						filters << Quickeebooks::Shared::Service::Filter.new(:text, :field => 'Name', :value => "#{full_name} - #{order.number}")

						found_custs = customer_service.list(filters)
						if found_custs.count == 1
							c = found_custs.entries.first
						end
					end

					invoice_service = Quickeebooks::Online::Service::Invoice.new
					invoice_service.access_token = oauth_client
					invoice_service.realm_id = qc.realm_id
					invoice_service.list

					invoice = Quickeebooks::Online::Model::Invoice.new
					# Create Line Items
					line_items = []
					order.line_items.each do |li|
						invoice_li = Quickeebooks::Online::Model::InvoiceLineItem.new
						invoice_li.desc = li.variant.product.name
						invoice_li.amount = li.price
						# invoice_li.item_id = Quickeebooks::Online::Model::Id.new(li.variant.product.id)
						invoice_li.unit_price = li.price
						invoice_li.quantity = li.quantity
						
						line_items << invoice_li
					end
					invoice.line_items = line_items

ap c
ap c.id
ap order
					# Create Header
					invoice_header = Quickeebooks::Online::Model::InvoiceHeader.new
					invoice_header.customer_id = Quickeebooks::Online::Model::Id.new(c.id)
					invoice_header.customer_name = c.name 
					# missionary bag class_id
					invoice_header.class_id = Quickeebooks::Online::Model::Id.new(100000000000344410)  
					invoice_header.ship_date = order.updated_at.time
					invoice_header.sub_total_amount = order.item_total
					invoice_header.total_amount = order.total
					invoice_header.txn_date = order.updated_at.time
					invoice_header.due_date = order.updated_at.time

					billing_address = Quickeebooks::Online::Model::Address.new
					billing_address.line1 = order.billing_address.address1
					billing_address.line2 = order.billing_address.address2
					billing_address.city = order.billing_address.city
					billing_address.country_sub_division_code = order.billing_address.state.name
					billing_address.country = order.billing_address.country.iso3
					billing_address.postal_code = order.billing_address.zipcode
					invoice_header.billing_address = billing_address
					
					shipping_address = Quickeebooks::Online::Model::Address.new
					shipping_address.line1 = order.shipping_address.address1
					shipping_address.line2 = order.shipping_address.address2
					shipping_address.city = order.shipping_address.city
					shipping_address.country_sub_division_code = order.shipping_address.state.name
					shipping_address.country = order.shipping_address.country.iso3
					shipping_address.postal_code = order.shipping_address.zipcode
					invoice_header.shipping_address = shipping_address

					invoice_header.bill_email = order.email
					invoice.header = invoice_header
					i = invoice_service.create(invoice)

					order.quickbooks_invoice_id = c.id
					unless o = order.save
						ap "There was a problems saving the order #{order.id} quickbooks_id."
					end

					# Register that a payment has gone through
					# Create payment
					payment_service = Quickeebooks::Online::Service::Payment.new
					payment_service.access_token = oauth_client
					payment_service.realm_id = qc.realm_id
					payment_service.list

					order.payments.where("quickbooks_id is null").each do |payment|
						qb_payment = Quickeebooks::Online::Model::Payment.new
						payment_header = Quickeebooks::Online::Model::PaymentHeader.new
						payment_header.txn_date = payment.created_at.time
						# payment_header.status = payment.state # Defaults to Paid
						payment_header.customer_id = Quickeebooks::Online::Model::Id.new(c.id) # ID
						payment_header.customer_name = c.name

						# payment_header.deposit_to_account_id = Quickeebooks::Online::Model::Id.new(?????)  #ID
						pmt_type = payment.source.cc_type
						case pmt_type
						when "visa"
							method_id = 3
							method_name = "Visa"
						when "discover"
							method_id = 7
							method_name = "Discover"
						when "master"
							method_id = 4
							method_name = "Mastercard"
						when "american_express"
							method_id = 5
							method_name = "American Express"
						end
						payment_header.payment_method_id = Quickeebooks::Online::Model::Id.new(method_id)  #ID
						payment_header.payment_method_name = method_name
						payment_header.total_amount = payment.amount
						# payment_header.detail =   #PaymentDetail (Card Details - Don't think we need this right now in Quickbooks)
						qb_payment.header = payment_header

						payment_line_items = []
						payment_detail = Quickeebooks::Online::Model::PaymentLineItem.new
						payment_detail.amount = payment.amount
						payment_detail.txn_id = i.id
						payment_line_items << payment_detail
						qb_payment.line_items = payment_line_items 

						p = payment_service.create(qb_payment)

						payment.update_attribute(:quickbooks_id, p.id)
					end

				end
				flash[:notice] = "XXXX Items exported to Quickbooks."
				redirect_to edit_admin_general_settings_path
			end
		end
	end
end
