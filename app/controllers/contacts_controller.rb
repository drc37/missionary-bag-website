class ContactsController < ApplicationController
  

  # GET /contacts/new
  # GET /contacts/new.json
  def new
    @contact = Contact.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contact }
    end
  end

  

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(contact_params)
    
    if @contact.save
      
      @contact = Contact.new()
      respond_to do |format|
        format.html { 
          flash[:notice] = 'Thanks for contacting us. We will get back with you shortly.'
          render action: "new" 
        }
        format.json { render json: { :response => "success" } }
      end
    else
      respond_to do |format|
        format.html { 
          flash[:notice] = 'There was a problem submitting the form. Please email us at support@missionarybag.com'
          render action: "new" 
        }
        format.json { 
          render json: {:response => "failed", :message => "Error: #{@contact.errors}" } 
        }
      end
      
      # render json: @contact.errors, status: :unprocessable_entity }
    end
    
  end

  private
    # Using a private method to encapsulate the permissible parameters is
    # just a good pattern since you'll be able to reuse the same permit
    # list between create and update. Also, you can specialize this method
    # with per-user checking of permissible attributes.
    def contact_params
      params.require(:contact).permit(:email, :message, :name, :phone, :marketing_message, :requested_contact)
    end
end
