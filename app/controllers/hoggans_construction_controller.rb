class HoggansConstructionController < ApplicationController
	before_filter :load_options

	def index
		@orders = Spree::Order.where("state = 'complete' AND NOT shipment_state	= 'shipped'").order("created_at ASC")
		# Create Manufacture Status for each item
		@orders.each do |order|
			order.line_items.each do |item|
				if !item.manufacture_status
					ManufactureStatus.create(:line_item => item)
				end
			end
		end
		@orders.reload

	end

	def update
		line_item_id = params[:id] 
		ap line_item_id

		line_item = Spree::LineItem.find(line_item_id)
		ap params
		ap params["manufacture_status"]
		ap params["manufacture_status"]["status"]
		line_item.manufacture_status.status = params["manufacture_status"]["status"]
		
		if line_item.save
			render :json => {:result => "success", :html => ""}
		else
			render :json => {:result => "failed", :html => ""}
		end
	end

	private

	def load_options
		@status_options = [
                              ['From Inventory', 'inventory'],
                              ['Acknowledged', 'acknowledged'],
                              ['Needs Embroidery', 'startEmbroidery'],
                              ['Getting Embroidery', 'gettingEmbroidery'],
                              ['Embroidery Done', 'embroideryDone'],
                              ['Begin Manufacture', 'begunManuf'],
                              ['Completed Manufacture', 'completedManuf'],
                              ['Delivered', 'delivered']
                            ]
	end
end