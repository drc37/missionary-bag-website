$(document).ready(function() {

	// Shipping form to get rates
	$("form.rates_form").ajaxForm({
		beforeSubmit: function(json) { 
		  $('.rate-table').fadeOut(); 
		  $('.table-loading').fadeIn(); 

		  // currentSelect = null;
		},
		target: '.rate-table', 
		success: 	function(json) { 
		  $('.rate-table').fadeIn(); 
		  $('.table-loading').fadeOut(); 
		  // currentSelect = null;
		},
		error: function(error){
			$(".rate-table").fadeOut();
			$('.table-loading').fadeOut(); 
		}

	}); 
});