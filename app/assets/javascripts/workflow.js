$(document).ready(function() {
	var currentSelect = null;

  // bind 'myForm' and provide a simple callback function
	$('form.edit_manufacture_status').ajaxForm({
		beforeSubmit: function(){
			// Show spinner

			currentSelect.after("<img class='spinner' src='/assets/spinner.gif'>");
		},
		success: 	function(json) {
							  $(".spinner").fadeOut(500, function() { $(this).remove(); });
							  currentSelect = null;
							}
	});

	$(".line-item-status select").on("change", function(event){
		// console.log("Selected");

		currentSelect = $(this);

		$(this).closest("form").submit();
	});
});
