$(document).ready(function() {

  $('.buy-now').on("click", function(event){
    // Check for an existing form, if not, then just take us to the general exam close function
    event.preventDefault();


    // console.log($(window).width());
    if($(window).width() < 700){
		window.location = "/store";
    } else {
    	$(".order-form-container").fadeIn();
    }

    return false;
  });

  // Add slider to home screen
 //  $('.banner').unslider({
	// 	speed: 500,               //  The speed to animate each slide (in milliseconds)
	// 	delay: 3000,              //  The delay between slide animations (in milliseconds)
	// 	complete: function() {},  //  A function that gets called after every slide animation
	// 	keys: true,               //  Enable keyboard (left, right) arrow shortcuts
	// 	dots: true,               //  Display dot navigation
	// 	fluid: false              //  Support responsive design. May break non-responsive designs
	// });

	$('.banner').bxSlider({
    controls: false,
    auto: true,
    pause: 5000,
    speed: 800,
    autoHover: true,
    easing: 'ease-in-out',
    video: true,
    autoDelay: 5000
  });

  $(".video-slide .play-button").fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		'width': 800,
        'height': 500,
		helpers : {
			media : {}
		},
        youtube : {
          autoplay: 0
        }
	});

  // $(".video").fitVids();

  $(".exterior-colors .color").on("click", function(event){
  	event.preventDefault();
  	if(!$(this).hasClass("big")){
	  	// Record the left position of this so we can put everything back where it needs to be
	  	$(this).attr("data-left", $(this).css("left"));

	  	// Fade out the other color buttons
	  	var id = $(this).attr("id");
	  	$(".exterior-colors .color").not("#" + id).removeClass("selected").fadeOut();
	  	//Move this to correct location
	  	var newLeft = "0px";
	  	if($(".interior-colors .selected.big").length > 0){
	  		newLeft = "191px";
	  		$(this).addClass("centered");
	  		$(".order-form-container .buttons").fadeIn();
	  	} else if($(".interior-colors .selected.big").length > 0){
	  		newLeft = "0px";
	  	}
	  	$(this).addClass("selected big").animate({left: newLeft}, 500, function(){
	  		$(".interior-colors").fadeIn();
	  	});

	  	// Change th titles
	  	$(".step.one").removeClass("bold");
	  	$(".step.two").addClass("bold");

	  	// Set the appropriate form values
	  	var prodID = $(this).data("prod-id");
	  	$("#product_id").attr("name", "products[" + prodID + "]");

	} else {
		// put everything back to where it should go
		$(".step.one").addClass("bold");
	  	$(".step.two").removeClass("bold");
		$(this).removeClass("big centered");
		$(".interior-colors").fadeOut();
		$(this).animate({ left: $(this).data("left") }, 500, function(){
			$(".exterior-colors .color").fadeIn();
		});
		$(".order-form-container .buttons").fadeOut();
	}
  	return false;
  });

  $(".interior-colors .color").on("click", function(event){
  	event.preventDefault();

  	if(!$(this).hasClass("big")){
  		// Record the left/top position of this so we can put everything back where it needs to be
	  	$(this).attr("data-left", $(this).css("left"));
	  	$(this).attr("data-top", $(this).css("top"));

	  	// Fade out the other color buttons
	  	var id = $(this).attr("id");
	  	$(".interior-colors .color").not("#" + id).removeClass("selected big").fadeOut();
	  	$(this).addClass("selected big");

	  	$(this).animate({
	  		left: "124px",
	  		top: "0px"
	  	}, 500);
	  	$(".exterior-colors .selected").addClass("centered").animate({ left: "191px" }, 500, function(){
	  		$(".order-form-container .buttons").fadeIn(); $(".color-location").fadeIn();
	  	});
	  	$("#" + id + " img").animate({width: "150px", height: "150px"}, 500);

	  	// Set form values
	  	// (should return color)
	  	var extColor = $(".exterior-colors .selected").data("color");
	  	// console.log(extColor);
	  	var intID = $(this).data(extColor);
	  	console.log(intID);
			$("#product_id").val(intID);
			$("#variant_id").val(intID);

			// Get the partial with the price and product availability
			$.get("home/" + $("#variant_id").val() + "/price",
				function(data){

					$(".price").html(data);
			});
		} else {
			//Move everything back to where it was
			$(this).removeClass("big");

			$(this).animate({
		  		left: $(this).data("left"),
		  		top: $(this).data("top")
		  	}, 500);
		  	$(".interior-colors .color").fadeIn();
		  	$(this).find("img").animate({width: "90px", height: "90px"}, 500);
		  	$(".exterior-colors .selected").removeClass("centered").animate({left: "0px"}, 500);
		  	$(".order-form-container .buttons").fadeOut();

		}
  	return false;
  });



  var currentSelect = null;
  $(".signup-form-container form#mc-embedded-subscribe-form").ajaxForm({
		beforeSubmit: function(){
			// Show spinner
			// currentSelect.after("<img class='spinner' src='/assets/spinner.gif'>");
			$.cookie("completed_signup", true);
		},
		success: 	function(json) {
		  $(".signup-form-container").fadeOut(500);
		  // currentSelect = null;
		},
		error: function(error){
			$(".signup-form-container").fadeOut(500);
		}

	});

	$(".signup-form-container .close-popover").click(function(e){
		$(".signup-form-container").fadeOut();
		$.cookie("completed_signup", false);
		$.cookie("dont_signup_show", true, { expires: 2 });
	});

	$(".order-form-container .close-popover").click(function(e){
		$(".order-form-container").fadeOut();
	});

	function showSignupForm(){
		// Check to see if cookie exists.
		var completed = $.cookie("completed_signup");
		var dont_show = $.cookie("dont_signup_show");

		if(!completed && !dont_show){
			$(".signup-form-container").delay(3000).fadeIn();
		}
	};

	showSignupForm();
  if(typeof map_data != 'undefined'){
    var max = 0,
      min = Number.MAX_VALUE,
      cc,
      startColor = [200, 238, 255],
      endColor = [0, 100, 145],
      colors = {},
      hex;

    //find maximum and minimum values
    for (cc in map_data)
    {
        if (parseFloat(map_data[cc]) > max)
        {
            max = parseFloat(map_data[cc]);
        }
        if (parseFloat(map_data[cc]) < min)
        {
            min = parseFloat(map_data[cc]);
        }
    }

    //set colors according to values of GDP
    for (cc in map_data)
    {
        if (map_data[cc] > 0)
        {
            colors[cc] = '#';
            for (var i = 0; i<3; i++)
            {
                hex = Math.round(startColor[i]
                    + (endColor[i]
                    - startColor[i])
                    * (map_data[cc] / (max - min))).toString(16);

                if (hex.length == 1)
                {
                    hex = '0'+hex;
                }

                colors[cc] += (hex.length == 1 ? '0' : '') + hex;
            }
        }
    }
    console.log(colors);

    jQuery('#vmap').vectorMap({
      map: 'world_mill_en',
      backgroundColor: '#fff',
      zoomOnScroll: false,
      series: {
        regions: [{
          values: map_data,
          scale: ['#C8EEFF', '#0071A4'],
          normalizeFunction: 'polynomial'
        }]
      },
      regionStyle: {
        initial: {
          fill: 'white',
          "fill-opacity": 1,
          stroke: '#ccc',
          "stroke-width": .3,
          "stroke-opacity": 1
        }

      }
     });
  };

});
