Query to get bag count:

total = 0; Spree::LineItem.joins([:order, :variant]).select("spree_variants.id as variant_id, spree_variants.sku, sum(quantity) as count").where("spree_orders.state = 'complete'").group("spree_variants.id, spree_variants.sku").order("sum(quantity) desc").each{ |v|  ap "Variant: #{v['variant_id']}-#{v['sku']} - Count - #{v['count']}"; total += v['count']}; ap total


Get mission Totals:

Spree::Order.select("mission, count(1) as count").where("NOT mission is NULL").group("mission").order("count(1)").each{|m| ap "#{m.count} - #{m.mission}"}